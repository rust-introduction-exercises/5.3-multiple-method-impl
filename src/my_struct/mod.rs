// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: Apache-2.0

pub struct MyStruct<'a> {
    pub foo : i32,
    pub bar : i32,
    pub msg : &'a str
}