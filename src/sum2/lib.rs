// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: Apache-2.0
use ::my_struct::MyStruct;

impl MyStruct<'_> {
    fn sum(&self) -> i32 {
        // geometric sum
        return std::sqrt(self.foo.powi(2) + self.bar.powi(2));
    }
}