// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: Apache-2.0
use ::my_struct::MyStruct;

fn main() {
    let _s: MyStruct = MyStruct{foo : 0, bar : -1, msg : "Hello"};

}