// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: Apache-2.0
use ::my_struct::MyStruct;

impl MyStruct<'_> {
    fn sum(&self) -> i32 {
        // linear sum
        return self.foo + self.bar;
    }
}